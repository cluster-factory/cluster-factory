# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/exoscale/exoscale" {
  version     = "0.41.1"
  constraints = "~> 0.41.0"
  hashes = [
    "h1:5csPLvZ4+PiK18vEAp7RNENmFm4oeJy0V7czr9mOctg=",
    "zh:3afa1893f20ad6d97fbbec03d6eb735f69f6c63655dc2f8adf550c165ada0c11",
    "zh:4259f18fc469f530c1a7dfe848a871a1f2ab50fb00f127272a45afe84993f714",
    "zh:63e5bc009d2bf53bb288fba15a07b4b68fe25cc3743f26834c473cf14224c431",
    "zh:6fb58e33194962b3d3590d6c05cab6a149e9e5d9bd927d346a976d4cc08cd2fa",
    "zh:7369aab15c033b1b1ce4338e1d7c17e98ee070f020f04bc3699875611bf9a35a",
    "zh:96b2995b5c3fc8e2ec544c6fcfc1a1783c0c84bd4cf784e5ef2bb861c719d622",
    "zh:b3b8083ca04c935b8f38f62ed2b2c3d2bc068630733f9b94030cb507dd80c540",
    "zh:bb5cfe0de5d03e043bbee8d87d85cbbd23dd12f77d609cf3647901ecf4a9da86",
    "zh:bc9ff6dbe7d154a0527ad90354b2e288a849787fc51b40a8bdfe1297b5cadc4d",
    "zh:c4db41ab97387ff8adc8f5e44a61089f14eb10561c70c6866ace816bfb5e035d",
    "zh:c5c120b1aaae615a9cae96d47a0d2132f7b28dc429d8192e441f9c48d1bfcc2e",
    "zh:cbaba932970c415435017509dd9c0cdb434e660d690ba684b771cab5005fd2ff",
    "zh:deda7bd5d36c488f41e6f8827d6b68d094024c0c24979664e414ce4b059135a3",
    "zh:ecd37f289f626f96c49ca0fdf375a42a14ca484f0c817c2a282a785d05098cdb",
  ]
}

provider "registry.terraform.io/volcano-coffee-company/cidr" {
  version     = "0.1.0"
  constraints = "0.1.0"
  hashes = [
    "h1:n4uTLP5Od/5JJ7ftF7GmV8FaTBnCMgVhM7aSgwqwL58=",
    "zh:10904ef8bda47973c45fc7d8e5ac835245dfb538985172296379278f243b43f6",
    "zh:16583dcccc68de2b7ae14a35ce7fe67b14060480bf080dd1c3671f2690812db1",
    "zh:176ca30769196ffa08063aefb002f504501619891981c50d4f474440d414925f",
    "zh:2dde7877a13701bd4e8ad5eed02b364ff847cc018b1fca934482605a37dc4a57",
    "zh:3ef8063a4b39f802a891a6602b9e88d2b184a864decb317fda5fe0261b0d441a",
    "zh:53a0881e38cdd1bf27ca7165a127ff8169a9948845c61af96ef130a9254201b1",
    "zh:815078cadf8218ffee32cd9b01e08959b1a054b4317452f6864f4f860dd92b05",
    "zh:84522b2c9baa7d589867acdc81e57cf84544db1c0723673cf819ee4c07a65bcb",
    "zh:985dc9ca6786236c3c345fcc6f520a9a72b2b31798b18fae1a62966c61baa0e4",
    "zh:adaa5d964971cb618b278de652d6588cc562771e9526d07616db57aa3dcf80d5",
    "zh:adf02611d3fa585543ed71658546bd0c913f17064a87bff5167acc8d76a6c18a",
    "zh:e31d30813d915b27f1bdc6aca79ce17cd6e28288d460a7a154f5704b6813f668",
  ]
}
